FROM amazoncorretto:17-alpine3.16
WORKDIR /app
COPY build/libs/chouette-0.0.1-SNAPSHOT.jar .
EXPOSE 8080
CMD ["java", "-jar", "chouette-0.0.1-SNAPSHOT.jar"]