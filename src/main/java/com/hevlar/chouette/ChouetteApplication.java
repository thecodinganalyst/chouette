package com.hevlar.chouette;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChouetteApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChouetteApplication.class, args);
    }

}
