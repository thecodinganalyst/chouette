package com.hevlar.chouette.controller;

import com.hevlar.chouette.controller.dto.AccountDto;
import com.hevlar.chouette.controller.dto.FinancialYearDto;
import com.hevlar.chouette.controller.dto.JournalEntryDto;
import com.hevlar.chouette.model.Account;
import com.hevlar.chouette.model.FinancialYear;
import com.hevlar.chouette.model.JournalEntry;
import com.hevlar.chouette.service.IAccountManager;
import com.hevlar.chouette.service.IChartOfAccounts;
import com.hevlar.chouette.service.IGeneralLedger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/accountManager")
public class AccountManagerController {
    IAccountManager accountManager;
    IChartOfAccounts chartOfAccounts;
    IGeneralLedger generalLedger;

    public AccountManagerController(
            IAccountManager accountManager,
            IChartOfAccounts chartOfAccounts,
            IGeneralLedger generalLedger
    ){
        this.accountManager = accountManager;
        this.chartOfAccounts = chartOfAccounts;
        this.generalLedger = generalLedger;
    }

    @PostMapping("/fy")
    @ResponseStatus(HttpStatus.CREATED)
    public FinancialYearDto newFinancialYear(@RequestBody FinancialYearDto financialYearDto){
        try{
            return accountManager.newFinancialYear(
                    financialYearDto.toFinancialYear()
            ).toFinancialYearDto();
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/fy")
    public List<FinancialYearDto> listFinancialYears(){
        return accountManager.listFinancialYears()
                .stream()
                .map(FinancialYear::toFinancialYearDto)
                .toList();
    }

    @GetMapping("/fy/{fy}")
    public FinancialYearDto getFinancialYear(@PathVariable String fy){
        return accountManager.getFinancialYear(fy)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
                .toFinancialYearDto();
    }

    @PostMapping("/account")
    @ResponseStatus(HttpStatus.CREATED)
    public AccountDto newAccount(@RequestBody AccountDto accountDto){
        try{
            return chartOfAccounts.addAccount(accountDto.toAccount())
                    .toAccountDto();
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{fy}/accounts")
    public List<AccountDto> listAccounts(@PathVariable String fy){
        Optional<FinancialYear> financialYear = accountManager.getFinancialYear(fy);
        if(financialYear.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Financial year " + fy + " not found");
        }
        return chartOfAccounts.listAccounts(fy)
                .stream()
                .map(Account::toAccountDto)
                .toList();
    }

    @GetMapping("/{fy}/accounts/{name}")
    public AccountDto getAccount(@PathVariable String fy, @PathVariable String name){
        return getAccountByFyAndAccountName(fy, name)
                .toAccountDto();
    }

    @PostMapping("/journal")
    @ResponseStatus(HttpStatus.CREATED)
    public JournalEntryDto newJournal(@RequestBody JournalEntryDto journalEntryDto){
        try{
            return generalLedger.addJournalEntry(journalEntryDto.toJournalEntry())
                    .toJournalEntryDto();
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{fy}/journals")
    @ResponseStatus(HttpStatus.OK)
    public List<JournalEntryDto> listJournals(@PathVariable String fy){
        try{
            return generalLedger.listJournalEntries(fy)
                    .stream()
                    .map(JournalEntry::toJournalEntryDto)
                    .toList();
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{fy}/journals/{accountName}")
    @ResponseStatus(HttpStatus.OK)
    public List<JournalEntryDto> listJournals(@PathVariable String fy, @PathVariable String accountName){
        Account account = getAccountByFyAndAccountName(fy, accountName);
        try{
            return generalLedger.listJournalEntries(account)
                    .stream()
                    .map(JournalEntry::toJournalEntryDto)
                    .toList();
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{fy}/journals/{fromDate}/{toDate}")
    @ResponseStatus(HttpStatus.OK)
    public List<JournalEntryDto> listJournals(@PathVariable String fy, @PathVariable LocalDate fromDate, @PathVariable LocalDate toDate){
        FinancialYear financialYear = accountManager.getFinancialYear(fy).orElseThrow();
        if(fromDate.isBefore(financialYear.getFromDate()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "From date cannot be before FY start date");
        if(toDate.isAfter(financialYear.getToDate()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "To date cannot be after FY end date");

        try{
            return generalLedger.listJournalEntries(fromDate, toDate)
                    .stream()
                    .map(JournalEntry::toJournalEntryDto)
                    .toList();
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{fy}/journals/{accountName}/{fromDate}/{toDate}")
    @ResponseStatus(HttpStatus.OK)
    public List<JournalEntryDto> listJournals(@PathVariable String fy, @PathVariable String accountName, @PathVariable LocalDate fromDate, @PathVariable LocalDate toDate){
        FinancialYear financialYear = accountManager.getFinancialYear(fy).orElseThrow();
        Account account = getAccountByFyAndAccountName(fy, accountName);
        if(fromDate.isBefore(financialYear.getFromDate()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "From date cannot be before FY start date");
        if(toDate.isAfter(financialYear.getToDate()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "To date cannot be after FY end date");

        try{
            return generalLedger.listJournalEntries(account, fromDate, toDate)
                    .stream()
                    .map(JournalEntry::toJournalEntryDto)
                    .toList();
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    private Account getAccountByFyAndAccountName(String fy, String accountName){
        Optional<FinancialYear> financialYear = accountManager.getFinancialYear(fy);
        if(financialYear.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Financial year " + fy + " not found");
        }
        return chartOfAccounts.getAccount(accountName, fy)
                .orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Account " + accountName + " not found in FY " + fy)
                );
    }

}
