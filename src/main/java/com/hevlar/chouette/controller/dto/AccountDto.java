package com.hevlar.chouette.controller.dto;

import com.hevlar.chouette.model.Account;
import com.hevlar.chouette.model.AccountGroup;

import java.math.BigDecimal;
import java.time.LocalDate;
public record AccountDto(
        String accountId,
        String name,
        AccountGroup accountGroup,
        LocalDate openingDate,
        String currency,
        BigDecimal openingBalance,
        String fy
) {
    public Account toAccount(){
        return new Account(accountId, name, accountGroup, openingDate, currency, openingBalance, fy);
    }
}
