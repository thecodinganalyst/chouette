package com.hevlar.chouette.controller.dto;

import com.hevlar.chouette.model.FinancialYear;

import java.time.LocalDate;

public record FinancialYearDto(
        String fy,
        LocalDate fromDate,
        LocalDate toDate
) {
    public FinancialYear toFinancialYear(){
        return new FinancialYear(fy, fromDate, toDate);
    }
}
