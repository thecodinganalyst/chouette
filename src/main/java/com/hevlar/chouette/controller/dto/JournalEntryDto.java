package com.hevlar.chouette.controller.dto;

import com.hevlar.chouette.model.JournalEntry;

import java.math.BigDecimal;
import java.time.LocalDate;

public record JournalEntryDto(
        Long journalId,
        LocalDate txDate,
        String description,
        String currency,
        BigDecimal amount,
        LocalDate postDate,
        String debitAccountId,
        String creditAccountId
) {
    public JournalEntry toJournalEntry(){
        return new JournalEntry(journalId, txDate, description, currency, amount, postDate, debitAccountId, creditAccountId);
    }
}
