package com.hevlar.chouette.model;

import com.hevlar.chouette.controller.dto.AccountDto;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;

@Entity
@NoArgsConstructor
@Data
public class Account{
    @Id
    private String accountId;
    private String name;
    private AccountGroup accountGroup;
    private Currency currency;
    private LocalDate openingDate;
    private BigDecimal openingBalance;
    private String fy;

    @ManyToOne
    @JoinColumn
    private FinancialYear financialYear;

    public boolean isIncomeStatementAccount(){
        return accountGroup.accountType == AccountType.INCOME_STATEMENT;
    }
    public boolean isBalanceSheetAccount() {
        return accountGroup.accountType == AccountType.BALANCE_SHEET;
    }
    public boolean isValidIncomeStatementAccount() {
        return fy == null && currency == null && openingBalance == null && openingDate == null;
    }
    public boolean isValidBalanceSheetAccount() {
        return fy != null && currency != null && openingBalance != null && openingDate != null;
    }

    public Account(String accountId, String name, AccountGroup accountGroup, LocalDate openingDate, String currency, BigDecimal openingBalance, String fy){
        this.accountId = accountId;
        this.name = name;
        this.accountGroup = accountGroup;
        this.openingDate = openingDate;
        this.currency = currency != null ? Currency.getInstance(currency) : null;
        this.openingBalance = openingBalance;
        this.fy = fy;
    }

    public AccountDto toAccountDto(){
        return new AccountDto(
                accountId,
                name,
                accountGroup,
                openingDate,
                currency != null ? currency.getCurrencyCode() : null,
                openingBalance,
                fy
        );
    }

}
