package com.hevlar.chouette.model;

import static com.hevlar.chouette.model.AccountType.BALANCE_SHEET;
import static com.hevlar.chouette.model.AccountType.INCOME_STATEMENT;
import static com.hevlar.chouette.model.EntryType.DEBIT;
import static com.hevlar.chouette.model.EntryType.CREDIT;

public enum AccountGroup {
    ASSET(DEBIT, null, BALANCE_SHEET),
    FIXED_ASSET(DEBIT, ASSET, BALANCE_SHEET),
    CURRENT_ASSET(DEBIT, ASSET, BALANCE_SHEET),
    LIABILITY(CREDIT, null, BALANCE_SHEET),
    LONG_TERM_LIABILITY(CREDIT, LIABILITY, BALANCE_SHEET),
    CURRENT_LIABILITY(CREDIT, LIABILITY, BALANCE_SHEET),
    REVENUE(CREDIT, null, INCOME_STATEMENT),
    EXPENSE(DEBIT, null, INCOME_STATEMENT),
    GAIN(CREDIT, null, INCOME_STATEMENT),
    LOSS(CREDIT, null, INCOME_STATEMENT),
    EQUITY(CREDIT, null, INCOME_STATEMENT);

    public final EntryType entryType;
    public final AccountGroup parent;

    public final AccountType accountType;

    AccountGroup(EntryType entryType, AccountGroup parent, AccountType accountType){
        this.entryType = entryType;
        this.parent = parent;
        this.accountType = accountType;
    }
}
