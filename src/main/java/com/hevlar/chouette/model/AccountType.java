package com.hevlar.chouette.model;

public enum AccountType {
    BALANCE_SHEET,
    INCOME_STATEMENT
}
