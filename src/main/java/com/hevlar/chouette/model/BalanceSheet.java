package com.hevlar.chouette.model;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Currency;

public record BalanceSheet(
        LocalDate balanceDate,
        Currency currency,
        Collection<BalanceSheetRow> assets,
        Collection<BalanceSheetRow> liabilities,
        Collection<BalanceSheetRow> equities
){ }
