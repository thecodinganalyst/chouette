package com.hevlar.chouette.model;

import java.math.BigDecimal;

public record BalanceSheetRow(Account account, BigDecimal amount) {
}
