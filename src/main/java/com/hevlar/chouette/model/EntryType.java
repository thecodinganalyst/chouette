package com.hevlar.chouette.model;

public enum EntryType {
    DEBIT,
    CREDIT
}
