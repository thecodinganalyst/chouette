package com.hevlar.chouette.model;

import com.hevlar.chouette.controller.dto.FinancialYearDto;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Collection;

@Entity
@Data
@NoArgsConstructor
public class FinancialYear{
    @Id
    private String fy;
    private LocalDate fromDate;
    private LocalDate toDate;
    @OneToMany(mappedBy = "financialYear")
    @PrimaryKeyJoinColumn
    private Collection<Account> accounts;

    public FinancialYearDto toFinancialYearDto(){
        return new FinancialYearDto(fy, fromDate, toDate);
    }

    public FinancialYear(String fy, LocalDate fromDate, LocalDate toDate){
        this.fy = fy;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }
}
