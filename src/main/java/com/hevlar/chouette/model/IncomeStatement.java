package com.hevlar.chouette.model;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Currency;

public record IncomeStatement(
        LocalDate from,
        LocalDate to,
        Currency currency,
        Collection<IncomeStatementRow> revenue,
        Collection<IncomeStatementRow> expenses,
        Collection<IncomeStatementRow> gains,
        Collection<IncomeStatementRow> loss
) {
}
