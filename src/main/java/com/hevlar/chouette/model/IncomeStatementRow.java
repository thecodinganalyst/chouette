package com.hevlar.chouette.model;

import java.math.BigDecimal;

public record IncomeStatementRow(Account account, BigDecimal amount) {
}
