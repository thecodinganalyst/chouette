package com.hevlar.chouette.model;

import com.hevlar.chouette.controller.dto.JournalEntryDto;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;

@Data
@NoArgsConstructor
@Entity
public class JournalEntry{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long journalId;

    private LocalDate txDate;

    private String description;

    private Currency currency;

    private BigDecimal amount;

    private LocalDate postDate;

    private String debitAccountId;

    private String creditAccountId;

    @JoinColumn
    @ManyToOne
    private Account debitAccount;

    @JoinColumn
    @ManyToOne
    private Account creditAccount;

    public JournalEntry(Long journalId, LocalDate txDate, String description, String currency, BigDecimal amount, LocalDate postDate, String debitAccountId, String creditAccountId){
        this.journalId = journalId;
        this.txDate = txDate;
        this.description = description;
        this.currency = Currency.getInstance(currency);
        this.amount = amount;
        this.postDate = postDate;
        this.debitAccountId = debitAccountId;
        this.creditAccountId = creditAccountId;
    }

    public JournalEntryDto toJournalEntryDto(){
        return new JournalEntryDto(
                journalId, txDate, description, currency.getCurrencyCode(), amount, postDate, debitAccountId, creditAccountId
        );
    }

}
