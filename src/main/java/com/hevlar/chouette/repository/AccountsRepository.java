package com.hevlar.chouette.repository;

import com.hevlar.chouette.model.Account;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface AccountsRepository extends JpaRepository<Account, String> {
    @Query("Select a from Account a where a.fy = :fy or a.fy is null")
    List<Account> listAccountsForFy(@Param("fy") String fy);
    @Query("Select a from Account a where a.name = :name and (a.fy = :fy or a.fy is null)")
    Optional<Account> getAccountForFy(@Param("name") String name, @Param("fy") String fy);
}
