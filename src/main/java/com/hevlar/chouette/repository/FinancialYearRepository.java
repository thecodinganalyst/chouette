package com.hevlar.chouette.repository;

import com.hevlar.chouette.model.FinancialYear;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
@Transactional
public interface FinancialYearRepository extends JpaRepository<FinancialYear, String> {
    Optional<FinancialYear> findByFromDateLessThanEqualAndToDateGreaterThanEqual(LocalDate fromDate, LocalDate toDate);
}
