package com.hevlar.chouette.repository;

import com.hevlar.chouette.model.JournalEntry;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
@Transactional
public interface JournalEntryRepository extends JpaRepository<JournalEntry, Long> {
    List<JournalEntry> findByTxDateBetween(LocalDate fromDate, LocalDate toDate);
    List<JournalEntry> findByDebitAccountIdOrCreditAccountId(String debitAccountId, String creditAccountId);
    @Query("Select j from JournalEntry j where (j.debitAccountId = :accountId or j.creditAccountId = :accountId) and (j.txDate >= :fromDate and j.txDate <= :toDate)")
    List<JournalEntry> findJournalEntriesFromAccountBetweenDates(
            @Param("accountId") String accountId,
            @Param("fromDate") LocalDate fromDate,
            @Param("toDate") LocalDate toDate);
}
