package com.hevlar.chouette.service;

import com.hevlar.chouette.model.FinancialYear;
import com.hevlar.chouette.repository.FinancialYearRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class AccountManager implements IAccountManager{

    private final FinancialYearRepository financialYearRepository;

    public AccountManager(FinancialYearRepository financialYearRepository){
        this.financialYearRepository = financialYearRepository;
    }

    @Override
    public FinancialYear newFinancialYear(FinancialYear financialYear) throws Exception {
        if(this.financialYearRepository.existsById(financialYear.getFy())) {
            throw new Exception("FY " + financialYear.getFy() + " already exists");
        }
        return this.financialYearRepository.save(financialYear);
    }

    @Override
    public List<FinancialYear> listFinancialYears() {
        return financialYearRepository.findAll();
    }

    @Override
    public Optional<FinancialYear> getFinancialYear(String fy) {
        return financialYearRepository.findById(fy);
    }

    @Override
    public Optional<FinancialYear> getFinancialYear(LocalDate date) {
        return financialYearRepository.findByFromDateLessThanEqualAndToDateGreaterThanEqual(date, date);
    }

    @Override
    public boolean canDeleteFinancialYear(String fy) {
        return false;
    }

    @Override
    public boolean deleteFinancialYear(String fy) {
        if(financialYearRepository.findById(fy).isEmpty()) throw new IllegalArgumentException("fy " + fy + " not found");
        if(!canDeleteFinancialYear(fy)) throw new IllegalArgumentException("Deleting fy " + fy + " is not allowed");
        financialYearRepository.deleteById(fy);
        return true;
    }
}
