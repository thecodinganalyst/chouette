package com.hevlar.chouette.service;

import com.hevlar.chouette.model.Account;
import com.hevlar.chouette.model.AccountType;
import com.hevlar.chouette.repository.AccountsRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ChartOfAccounts implements IChartOfAccounts{

    private final AccountsRepository accountsRepository;
    private final AccountManager accountManager;

    public ChartOfAccounts(AccountsRepository accountsRepository, AccountManager accountManager){
        this.accountsRepository = accountsRepository;
        this.accountManager = accountManager;
    }

    @Override
    public Account addAccount(Account account) throws Exception {
        String accountId = generateAccountId(account);

        if(account.isIncomeStatementAccount() && !account.isValidIncomeStatementAccount()){
            throw new Exception(account.getAccountGroup().accountType.name() + " account should not have currency, opening date, opening balance or fy");
        }
        if(account.isBalanceSheetAccount() && !account.isValidBalanceSheetAccount()){
            throw new Exception(account.getAccountGroup().accountType.name() + " account should have currency, opening date, opening balance and fy");
        }

        if(account.isBalanceSheetAccount() && accountManager.getFinancialYear(account.getFy()).isEmpty()){
            throw new Exception("Financial Year " + account.getFy() + " does not exists");
        }

        if(accountsRepository.getAccountForFy(account.getName(), account.getFy()).isPresent()){
            if(account.isBalanceSheetAccount())
                throw new Exception("Account with name - " + account.getName() + " already exists in FY " + account.getFy());
            else
                throw new Exception("Account with name - " + account.getName() + " already exists");
        }

        account.setAccountId(accountId);
        return accountsRepository.save(account);
    }

    private static String generateAccountId(Account account) {
        String accountId = account.getName();
        if(account.getAccountGroup().accountType == AccountType.BALANCE_SHEET){
            accountId = account.getFy() + "_" + accountId;
        }
        return accountId;
    }

    @Override
    public boolean canEditAccount(String id) {
        return false;
    }

    @Override
    public Account editAccount(Account account) throws Exception {
        if(canEditAccount(account.getAccountId())){
            throw new Exception("Account " + account.getAccountId() + " cannot be edited");
        }
        return accountsRepository.save(account);
    }

    @Override
    public boolean deleteAccount(String id) throws Exception {
        if(canEditAccount(id)){
            throw new Exception("Account " + id + " cannot be edited");
        }
        return false;
    }

    @Override
    public List<Account> listAccounts(String fy) {
        return accountsRepository.listAccountsForFy(fy);
    }

    @Override
    public Optional<Account> getAccount(String id) {
        return accountsRepository.findById(id);
    }

    @Override
    public Optional<Account> getAccount(String name, String fy) {
        return accountsRepository.getAccountForFy(name, fy);
    }
}
