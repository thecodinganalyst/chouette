package com.hevlar.chouette.service;

import com.hevlar.chouette.model.Account;
import com.hevlar.chouette.model.FinancialYear;
import com.hevlar.chouette.model.JournalEntry;
import com.hevlar.chouette.repository.JournalEntryRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class GeneralLedger implements IGeneralLedger {

    private final JournalEntryRepository journalEntryRepository;
    private final IAccountManager accountManager;

    private final IChartOfAccounts chartOfAccounts;

    public GeneralLedger(
            JournalEntryRepository journalEntryRepository,
            IAccountManager accountManager,
            IChartOfAccounts chartOfAccounts){

        this.journalEntryRepository = journalEntryRepository;
        this.accountManager = accountManager;
        this.chartOfAccounts = chartOfAccounts;
    }

    @Override
    public JournalEntry addJournalEntry(JournalEntry journalEntry) throws Exception {
        validateJournalEntry(journalEntry);
        return journalEntryRepository.save(journalEntry);
    }

    @Override
    public boolean canEditJournalEntry(Long id) {
        return false;
    }

    @Override
    public JournalEntry editJournalEntry(JournalEntry journalEntry) throws Exception {
        if(!canEditJournalEntry(journalEntry.getJournalId())){
            throw new Exception("Journal Entry cannot be edited");
        }
        return journalEntryRepository.save(journalEntry);
    }

    @Override
    public boolean deleteJournalEntry(Long id) throws Exception {
        if(!canEditJournalEntry(id)){
            throw new Exception("Journal Entry cannot be deleted");
        }
        journalEntryRepository.deleteById(id);
        return true;
    }

    @Override
    public List<JournalEntry> listJournalEntries(String fy) {
        FinancialYear financialYear = accountManager.getFinancialYear(fy).orElseThrow();
        return journalEntryRepository.findByTxDateBetween(financialYear.getFromDate(), financialYear.getToDate());
    }

    @Override
    public List<JournalEntry> listJournalEntries(Account account) {
        return journalEntryRepository.findByDebitAccountIdOrCreditAccountId(account.getAccountId(), account.getAccountId());
    }

    @Override
    public List<JournalEntry> listJournalEntries(LocalDate fromDate, LocalDate toDate) {
        return journalEntryRepository.findByTxDateBetween(fromDate, toDate);
    }

    @Override
    public List<JournalEntry> listJournalEntries(Account account, LocalDate fromDate, LocalDate toDate) {
        return journalEntryRepository.findJournalEntriesFromAccountBetweenDates(account.getAccountId(), fromDate, toDate);
    }

    @Override
    public Optional<JournalEntry> getJournalEntry(Long id) {
        return journalEntryRepository.findById(id);
    }

    private void validateJournalEntry(JournalEntry journalEntry) throws Exception {

        if(Objects.equals(journalEntry.getDebitAccountId(), journalEntry.getCreditAccountId())){
            throw new Exception("Debit account and credit account must be different");
        }

        FinancialYear financialYear = accountManager.getFinancialYear(journalEntry.getTxDate())
                .orElseThrow(() -> new Exception("Financial year not found for tx date " + journalEntry.getTxDate().toString())
                );

        boolean debitAccountValid = false;
        boolean creditAccountValid = false;
        LocalDate debitAccountOpeningDate = null;
        LocalDate creditAccountOpeningDate = null;
        List<Account> validAccounts = chartOfAccounts.listAccounts(financialYear.getFy());
        for (Account account : validAccounts) {
            if(Objects.equals(account.getAccountId(), journalEntry.getDebitAccountId())) {
                debitAccountValid = true;
                debitAccountOpeningDate = account.getOpeningDate();
            }
            if(Objects.equals(account.getAccountId(), journalEntry.getCreditAccountId())) {
                creditAccountValid = true;
                creditAccountOpeningDate = account.getOpeningDate();
            }
        }

        if (!debitAccountValid && !creditAccountValid){
            throw new Exception("Both debit account and credit account are not in the same fy as the tx date");
        }else if (debitAccountValid && !creditAccountValid) {
            throw new Exception("Credit account not in the same fy as the tx date");
        } else if (!debitAccountValid && creditAccountValid) {
            throw new Exception("Debit account not in the same fy as the tx date");
        }

        if(debitAccountOpeningDate != null && journalEntry.getTxDate().isBefore(debitAccountOpeningDate)
                && creditAccountOpeningDate != null && journalEntry.getTxDate().isBefore(creditAccountOpeningDate)){
            throw new Exception("TxDate before debit account and credit account opening date");
        }else if(debitAccountOpeningDate != null && journalEntry.getTxDate().isBefore(debitAccountOpeningDate)){
            throw new Exception("TxDate before debit account opening date");
        }else if(creditAccountOpeningDate != null && journalEntry.getTxDate().isBefore(creditAccountOpeningDate)){
            throw new Exception("TxDate before credit account opening date");
        }
    }
}
