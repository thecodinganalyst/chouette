package com.hevlar.chouette.service;

import com.hevlar.chouette.model.FinancialYear;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface IAccountManager {

    FinancialYear newFinancialYear(FinancialYear financialYear) throws Exception;

    List<FinancialYear> listFinancialYears();

    Optional<FinancialYear> getFinancialYear(String fy);

    Optional<FinancialYear> getFinancialYear(LocalDate date);

    boolean canDeleteFinancialYear(String fy);

    boolean deleteFinancialYear(String fy);
}
