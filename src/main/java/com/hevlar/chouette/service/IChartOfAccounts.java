package com.hevlar.chouette.service;

import com.hevlar.chouette.model.Account;

import java.util.List;
import java.util.Optional;

public interface IChartOfAccounts {
    Account addAccount(Account account) throws Exception;
    boolean canEditAccount(String id);
    Account editAccount(Account account) throws Exception;
    boolean deleteAccount(String id) throws Exception;
    List<Account> listAccounts(String fy);
    Optional<Account> getAccount(String id);
    Optional<Account> getAccount(String name, String fy);
}
