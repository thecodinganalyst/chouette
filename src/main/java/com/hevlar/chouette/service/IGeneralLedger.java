package com.hevlar.chouette.service;

import com.hevlar.chouette.model.Account;
import com.hevlar.chouette.model.JournalEntry;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface IGeneralLedger {
    JournalEntry addJournalEntry(JournalEntry journalEntry) throws Exception;
    boolean canEditJournalEntry(Long id);
    JournalEntry editJournalEntry(JournalEntry journalEntry) throws Exception;
    boolean deleteJournalEntry(Long id) throws Exception;
    List<JournalEntry> listJournalEntries(String fy);
    List<JournalEntry> listJournalEntries(Account account);
    List<JournalEntry> listJournalEntries(LocalDate fromDate, LocalDate toDate);
    List<JournalEntry> listJournalEntries(Account account, LocalDate fromDate, LocalDate toDate);
    Optional<JournalEntry> getJournalEntry(Long id);
}

