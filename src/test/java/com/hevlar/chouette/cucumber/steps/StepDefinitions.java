package com.hevlar.chouette.cucumber.steps;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.Map;
import java.util.Objects;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class StepDefinitions {
    @Autowired
    MockMvc mockMvc;

    ObjectMapper objectMapper = JsonMapper.builder().findAndAddModules().build();

    ResultActions resultActions;

    Map<String, String> dataTypeToCreateEndpoint = Map.of(
            "financial year", "/accountManager/fy",
            "account", "/accountManager/account",
            "journal entry", "/accountManager/journal"
    );

    @Given("the following {string} data list exists")
    public void the_following_data_list_exists(String dataType, DataTable dataTable) throws Exception {
        String endpoint = dataTypeToCreateEndpoint.get(dataType);
        var dataList = dataTable.asMaps();
        for (var data : dataList) {
            String json = objectMapper.writeValueAsString(data);
            this.mockMvc.perform(
                    callApi(endpoint, "POST")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(json)
            );
        }
    }

    @When("{string} is called with {string}")
    public void api_is_called_with_method(String endpoint, String method) throws Exception {
        this.resultActions = this.mockMvc.perform(
                callApi(endpoint, method)
                        .contentType(MediaType.APPLICATION_JSON)
        );
    }

    @When("{string} is called with {string} with the following data")
    public void api_is_called_with_the_following_data(String endpoint, String method, DataTable dataTable) throws Exception {
        String json = objectMapper.writeValueAsString(dataTable.asMap());
        this.resultActions = this.mockMvc.perform(
                callApi(endpoint, method)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        );
    }
    @Then("HttpStatus {int} is expected")
    public void http_status_is_expected(Integer statusCode) throws Exception {
        this.resultActions.andExpect(status().is(statusCode));
    }
    @Then("the following data is returned")
    public void the_following_data_is_returned(DataTable dataTable) throws Exception {
        var data = dataTable.asMap();
        this.resultActions.andExpect(
            content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)
        );
        var keys = data.keySet();
        for (String key : keys) {
            this.resultActions.andExpect(
                    jsonPath("$." + key).value(data.get(key))
            );
        }
    }

    @Then("the following data list is returned")
    public void the_following_data_list_is_returned(DataTable dataTable) throws Exception {
        var dataList = dataTable.asMaps();
        this.resultActions.andExpectAll(
                content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON),
                jsonPath("$.length()").value(dataList.size())
        );
        for (int i = 0; i < dataList.size(); i++) {
            var data = dataList.get(i);
            var keys = data.keySet();
            for (String key : keys) {
                this.resultActions.andExpect(
                        jsonPath("$[" + i + "]." + key).value(data.get(key))
                );
            }
        }
    }

    private MockHttpServletRequestBuilder callApi(String route, String method){
        String apiVersionRoute = "/api/v1" + route;
        return switch (method) {
            case "GET" -> get(apiVersionRoute);
            case "POST" -> post(apiVersionRoute);
            case "PUT" -> put(apiVersionRoute);
            case "DELETE" -> delete(apiVersionRoute);
            default -> throw new IllegalArgumentException("Method " + method + " is not supported");
        };
    }

    @And("exception message should be {string}")
    public void exceptionMessageShouldBe(String message) throws Exception {
        this.resultActions.andExpectAll(
                (result) -> assertThat(Objects.requireNonNull(result.getResolvedException()).getMessage(), equalTo(message))
        );
    }

    @Then("exception message should be empty")
    public void exceptionMessageShouldBeEmpty() throws Exception {
        this.resultActions.andExpect(
                (result) -> assertThat(result.getResolvedException(), is(nullValue()))
        );
    }
}
