Feature: Start a new financial year
  Create a new financial year to start accounting

  Scenario: Brand new financial year
    When "/accountManager/fy" is called with "POST" with the following data
      | fy       | 2023       |
      | fromDate | 2023-01-01 |
      | toDate   | 2023-12-31 |
    Then HttpStatus 201 is expected
    And the following data is returned
      | fy       | 2023       |
      | fromDate | 2023-01-01 |
      | toDate   | 2023-12-31 |

  Scenario: Duplicate year will result in failure
    Given the following "financial year" data list exists
      | fy    | fromDate    | toDate      |
      | 2023  | 2023-01-01  | 2023-12-31  |
    When "/accountManager/fy" is called with "POST" with the following data
      | fy       | 2023       |
      | fromDate | 2023-01-01 |
      | toDate   | 2023-12-31 |
    Then HttpStatus 409 is expected