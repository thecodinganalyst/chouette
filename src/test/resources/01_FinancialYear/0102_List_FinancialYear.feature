Feature: List financial years available

  Background: Existing financial year data
    Given the following "financial year" data list exists
      | fy    | fromDate    | toDate      |
      | 2023  | 2023-01-01  | 2023-12-31  |

  Scenario: List financial years in database
    When "/accountManager/fy" is called with "GET"
    Then HttpStatus 200 is expected
    And the following data list is returned
      | fy   | fromDate   | toDate     |
      | 2023 | 2023-01-01 | 2023-12-31 |