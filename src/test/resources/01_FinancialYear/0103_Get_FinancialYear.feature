Feature: Retrieve a particular financial year

  Background: Existing financial year data
    Given the following "financial year" data list exists
      | fy    | fromDate    | toDate      |
      | 2023  | 2023-01-01  | 2023-12-31  |

  Scenario: Get financial year
    When "/accountManager/fy/2023" is called with "GET"
    Then HttpStatus 200 is expected
    And the following data is returned
      | fy       | 2023       |
      | fromDate | 2023-01-01 |
      | toDate   | 2023-12-31 |

  Scenario: Get non existent financial year
    When "/accountManager/fy/2022" is called with "GET"
    Then HttpStatus 404 is expected