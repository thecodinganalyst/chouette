Feature: Creating new accounts
  Create new accounts

  Background: Existing accounts in the database
    Given the following "financial year" data list exists
      | fy    | fromDate    | toDate      |
      | 2023  | 2023-01-01  | 2023-12-31  |

  Rule: Balance sheet accounts need to have all the fields
    Scenario: Brand new balance sheet account
      When "/accountManager/account" is called with "POST" with the following data
        | name            | Cash       |
        | accountGroup    | ASSET      |
        | currency        | SGD        |
        | openingDate     | 2023-01-01 |
        | openingBalance  | 100        |
        | fy              | 2023       |
      Then HttpStatus 201 is expected
      And the following data is returned
        | accountId       | 2023_Cash  |
        | name            | Cash       |
        | accountGroup    | ASSET      |
        | currency        | SGD        |
        | openingDate     | 2023-01-01 |
        | openingBalance  | 100        |
        | fy              | 2023       |

    Scenario: Brand new balance sheet account with insufficient field data
      When "/accountManager/account" is called with "POST" with the following data
        | name            | CreditCard |
        | accountGroup    | ASSET      |
        | currency        | SGD        |
        | openingDate     | 2023-01-01 |
        | openingBalance  | 100        |
        | fy              |            |
      Then HttpStatus 400 is expected
      And exception message should be "400 BAD_REQUEST \"BALANCE_SHEET account should have currency, opening date, opening balance and fy\""

    Scenario: Brand new balance sheet account with non existent financial year
      When "/accountManager/account" is called with "POST" with the following data
        | name            | CreditCard |
        | accountGroup    | ASSET      |
        | currency        | SGD        |
        | openingDate     | 2023-01-01 |
        | openingBalance  | 100        |
        | fy              | 2022       |
      Then HttpStatus 400 is expected
      And exception message should be "400 BAD_REQUEST \"Financial Year 2022 does not exists\""


  Rule: Income Statement accounts should only have name and account group
    Scenario: Brand new income statement account
      When "/accountManager/account" is called with "POST" with the following data
        | name            | Food       |
        | accountGroup    | EXPENSE    |
        | currency        |            |
        | openingDate     |            |
        | openingBalance  |            |
        | fy              |            |
      Then exception message should be empty
      And HttpStatus 201 is expected
      And the following data is returned
        | accountId       | Food       |
        | name            | Food       |
        | accountGroup    | EXPENSE    |
        | currency        |            |
        | openingDate     |            |
        | openingBalance  |            |
        | fy              |            |

    Scenario: Brand new income statement account with additional data not required
      When "/accountManager/account" is called with "POST" with the following data
        | name            | Food       |
        | accountGroup    | EXPENSE    |
        | currency        |            |
        | openingDate     |            |
        | openingBalance  |            |
        | fy              | 2023       |
      Then HttpStatus 400 is expected
      And exception message should be "400 BAD_REQUEST \"INCOME_STATEMENT account should not have currency, opening date, opening balance or fy\""

  Rule: Name of balance sheet accounts must be unique for each FY
    Scenario: Duplicate name in same fy will result in failure
      Given the following "account" data list exists
        | name   | accountGroup  | currency | openingDate | openingBalance  | fy   |
        | Cash   | ASSET         | SGD      | 2023-01-01  | 100.0           | 2023 |
      When "/accountManager/account" is called with "POST" with the following data
        | name            | Cash       |
        | accountGroup    | ASSET      |
        | currency        | SGD        |
        | openingDate     | 2023-01-01 |
        | openingBalance  | 100        |
        | fy              | 2023       |
      Then HttpStatus 400 is expected

    Scenario: Balance sheet account with same name in another FY
      Given the following "account" data list exists
        | name   | accountGroup  | currency | openingDate | openingBalance  | fy   |
        | Cash   | ASSET         | SGD      | 2022-01-01  | 100.0           | 2022 |
      When "/accountManager/account" is called with "POST" with the following data
        | name            | Cash       |
        | accountGroup    | ASSET      |
        | currency        | SGD        |
        | openingDate     | 2023-01-01 |
        | openingBalance  | 100        |
        | fy              | 2023       |
      Then HttpStatus 201 is expected
      And the following data is returned
        | accountId       | 2023_Cash  |
        | name            | Cash       |
        | accountGroup    | ASSET      |
        | currency        | SGD        |
        | openingDate     | 2023-01-01 |
        | openingBalance  | 100        |
        | fy              | 2023       |

  Rule: Name of income statement accounts must be unique regardless of FY
    Scenario: Duplicate name for income statement account will result in failure
      Given the following "account" data list exists
        | name   | accountGroup  | currency | openingDate | openingBalance  | fy   |
        | Food   | EXPENSE       |          |             |                 |      |
      When "/accountManager/account" is called with "POST" with the following data
        | name            | Food       |
        | accountGroup    | EXPENSE    |
        | currency        |            |
        | openingDate     |            |
        | openingBalance  |            |
        | fy              |            |
      Then HttpStatus 400 is expected

  Rule: Balance sheet account should not have the same name as any other income statement account
    Scenario: Duplicate name of income statement account in same fy will result in failure
      Given the following "account" data list exists
        | name   | accountGroup  | currency | openingDate | openingBalance  | fy   |
        | Food   | EXPENSE       |          |             |                 |      |
      When "/accountManager/account" is called with "POST" with the following data
        | name            | Food       |
        | accountGroup    | ASSET      |
        | currency        | SGD        |
        | openingDate     | 2023-01-01 |
        | openingBalance  | 100.0      |
        | fy              | 2023       |
      Then HttpStatus 400 is expected