Feature: Get a particular account

  Background: Existing accounts in the database
    Given the following "financial year" data list exists
      | fy    | fromDate    | toDate      |
      | 2023  | 2023-01-01  | 2023-12-31  |
    Given the following "account" data list exists
      | name   | accountGroup  | currency | openingDate | openingBalance  | fy   |
      | Cash   | ASSET         | SGD      | 2023-01-01  | 100.0           | 2023 |

  Scenario: Get account by name and fy
    When "/accountManager/2023/accounts/Cash" is called with "GET"
    Then HttpStatus 200 is expected
    And the following data is returned
      | name            | Cash       |
      | accountGroup    | ASSET      |
      | currency        | SGD        |
      | openingDate     | 2023-01-01 |
      | openingBalance  | 100.0      |
      | fy              | 2023       |