Feature: Creating new journal entries
  Create new journal entry

  Background: Existing accounts in the database
    Given the following "financial year" data list exists
      | fy    | fromDate    | toDate      |
      | 2023  | 2023-01-01  | 2023-12-31  |
      | 2022  | 2022-01-01  | 2022-12-31  |
    Given the following "account" data list exists
      | accountId | name      | accountGroup  | currency | openingDate | openingBalance  | fy   |
      | 2023_Cash | Cash      | ASSET         | SGD      | 2023-01-02  | 100.0           | 2023 |
      | Food      | Food      | EXPENSE       |          |             |                 |      |
      | 2022_Cash | Cash      | ASSET         | SGD      | 2022-01-02  | 100.0           | 2022 |
      | Transport | Transport | EXPENSE       |          |             |                 |      |
      | 2023_Bank | Bank      | ASSET         | SGD      | 2023-01-02  | 1000.0          | 2023 |
      | 2022_Bank | Bank      | ASSET         | SGD      | 2022-01-02  | 1000.0          | 2022 |

  Scenario: Brand new journal entry
    When "/accountManager/journal" is called with "POST" with the following data
      | txDate            | 2023-01-02 |
      | description       | breakfast  |
      | currency          | SGD        |
      | amount            | 10         |
      | postDate          | 2023-01-02 |
      | debitAccountId    | Food       |
      | creditAccountId   | 2023_Cash  |
    Then exception message should be empty
    And HttpStatus 201 is expected
    And the following data is returned
      | txDate            | 2023-01-02 |
      | description       | breakfast  |
      | currency          | SGD        |
      | amount            | 10         |
      | postDate          | 2023-01-02 |
      | debitAccountId    | Food       |
      | creditAccountId   | 2023_Cash  |

  Rule: txDate must fall within an existing fy
    Scenario: New journal entry with txDate not in any existing fy is not allowed
      When "/accountManager/journal" is called with "POST" with the following data
        | txDate            | 2024-01-02 |
        | description       | breakfast  |
        | currency          | SGD        |
        | amount            | 10         |
        | postDate          | 2024-01-02 |
        | debitAccountId    | Food       |
        | creditAccountId   | 2023_Cash  |
      Then HttpStatus 400 is expected
      And exception message should be "400 BAD_REQUEST \"Financial year not found for tx date 2024-01-02\""

  Rule: debitAccountId and creditAccountId must be existing account ids within the same fy of the txDate
    Scenario: New journal entry with debit account not in FY of txDate is not allowed
      When "/accountManager/journal" is called with "POST" with the following data
        | txDate            | 2023-01-02 |
        | description       | transfer   |
        | currency          | SGD        |
        | amount            | 10         |
        | postDate          | 2023-01-02 |
        | debitAccountId    | 2022_Cash  |
        | creditAccountId   | 2023_Cash  |
      Then HttpStatus 400 is expected
      And exception message should be "400 BAD_REQUEST \"Debit account not in the same fy as the tx date\""

    Scenario: New journal entry with credit account not in FY of txDate is not allowed
      When "/accountManager/journal" is called with "POST" with the following data
        | txDate            | 2023-01-02 |
        | description       | breakfast  |
        | currency          | SGD        |
        | amount            | 10         |
        | postDate          | 2023-01-02 |
        | debitAccountId    | Food       |
        | creditAccountId   | 2022_Cash  |
      Then HttpStatus 400 is expected
      And exception message should be "400 BAD_REQUEST \"Credit account not in the same fy as the tx date\""

    Scenario: New journal entry with both accounts not in FY of txDate is not allowed
      When "/accountManager/journal" is called with "POST" with the following data
        | txDate            | 2023-01-02 |
        | description       | breakfast  |
        | currency          | SGD        |
        | amount            | 10         |
        | postDate          | 2023-01-02 |
        | debitAccountId    | 2022_Cash  |
        | creditAccountId   | 2022_Bank  |
      Then HttpStatus 400 is expected
      And exception message should be "400 BAD_REQUEST \"Both debit account and credit account are not in the same fy as the tx date\""

  Rule: txDate cannot be before the opening dates of both credit account id and debit account id
    Scenario: txDate is before opening date of credit account id
      When "/accountManager/journal" is called with "POST" with the following data
        | txDate            | 2023-01-01 |
        | description       | breakfast  |
        | currency          | SGD        |
        | amount            | 10         |
        | postDate          | 2023-01-01 |
        | debitAccountId    | Food       |
        | creditAccountId   | 2023_Cash  |
      Then HttpStatus 400 is expected
      And exception message should be "400 BAD_REQUEST \"TxDate before credit account opening date\""

    Scenario: txDate is before opening date of debit account id
      When "/accountManager/journal" is called with "POST" with the following data
        | txDate            | 2023-01-01 |
        | description       | refund     |
        | currency          | SGD        |
        | amount            | 10         |
        | postDate          | 2023-01-01 |
        | debitAccountId    | 2023_Cash  |
        | creditAccountId   | Food       |
      Then HttpStatus 400 is expected
      And exception message should be "400 BAD_REQUEST \"TxDate before debit account opening date\""

    Scenario: txDate is before opening date of both debit account id and credit account id
      When "/accountManager/journal" is called with "POST" with the following data
        | txDate            | 2023-01-01 |
        | description       | withdrawal |
        | currency          | SGD        |
        | amount            | 10         |
        | postDate          | 2023-01-01 |
        | debitAccountId    | 2023_Cash  |
        | creditAccountId   | 2023_Bank  |
      Then HttpStatus 400 is expected
      And exception message should be "400 BAD_REQUEST \"TxDate before debit account and credit account opening date\""

  Rule: debit account id and credit account id cannot be the same
    Scenario: debit account id and credit account id are the same
      When "/accountManager/journal" is called with "POST" with the following data
        | txDate            | 2023-01-01 |
        | description       | breakfast  |
        | currency          | SGD        |
        | amount            | 10         |
        | postDate          | 2023-01-01 |
        | debitAccountId    | 2023_Cash  |
        | creditAccountId   | 2023_Cash  |
      Then HttpStatus 400 is expected
      And exception message should be "400 BAD_REQUEST \"Debit account and credit account must be different\""