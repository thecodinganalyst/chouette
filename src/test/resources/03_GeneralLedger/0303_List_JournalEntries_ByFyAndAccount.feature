Feature: List journal entries by Account
  List journal entries by Accounts

  Background: Existing accounts in the database
    Given the following "financial year" data list exists
      | fy    | fromDate    | toDate      |
      | 2022  | 2022-01-01  | 2022-12-31  |
      | 2023  | 2023-01-01  | 2023-12-31  |
    Given the following "account" data list exists
      | accountId | name         | accountGroup  | currency | openingDate | openingBalance  | fy   |
      | 2023_Cash | Cash         | ASSET         | SGD      | 2023-01-01  | 100.0           | 2023 |
      | Food      | Food         | EXPENSE       |          |             |                 |      |
      | 2022_Cash | Cash         | ASSET         | SGD      | 2022-01-01  | 100.0           | 2022 |

    Given the following "journal entry" data list exists
      | journalId | txDate     | description | currency | amount | postDate   | debitAccountId | creditAccountId |
      | 1         | 2023-02-01 | breakfast   | SGD      | 10.0   | 2023-02-01 | Food           | 2023_Cash       |
      | 2         | 2023-02-01 | lunch       | SGD      | 12.0   | 2023-02-01 | Food           | 2023_Cash       |
      | 3         | 2023-02-01 | dinner      | SGD      | 15.0   | 2023-02-01 | Food           | 2023_Cash       |
      | 4         | 2022-02-01 | breakfast   | SGD      | 10.0   | 2022-02-01 | Food           | 2022_Cash       |
      | 5         | 2022-02-01 | lunch       | SGD      | 12.0   | 2022-02-01 | Food           | 2022_Cash       |
      | 6         | 2022-02-01 | dinner      | SGD      | 15.0   | 2022-02-01 | Food           | 2022_Cash       |
      | 7         | 2023-02-01 | refund      | SGD      | 5.0    | 2023-02-01 | 2023_Cash      | Food            |

  Scenario: List journal entries for 2023 Cash
    When "/accountManager/2023/journals/Cash" is called with "GET"
    Then exception message should be empty
    And HttpStatus 200 is expected
    And the following data list is returned
      | journalId | txDate     | description | currency | amount | postDate   | debitAccountId | creditAccountId |
      | 1         | 2023-02-01 | breakfast   | SGD      | 10.0   | 2023-02-01 | Food           | 2023_Cash       |
      | 2         | 2023-02-01 | lunch       | SGD      | 12.0   | 2023-02-01 | Food           | 2023_Cash       |
      | 3         | 2023-02-01 | dinner      | SGD      | 15.0   | 2023-02-01 | Food           | 2023_Cash       |
      | 7         | 2023-02-01 | refund      | SGD      | 5.0    | 2023-02-01 | 2023_Cash      | Food            |