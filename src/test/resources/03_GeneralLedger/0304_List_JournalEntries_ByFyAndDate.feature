Feature: List journal entries by Date
  List journal entries by Date

  Background: Existing accounts in the database
    Given the following "financial year" data list exists
      | fy    | fromDate    | toDate      |
      | 2023  | 2023-01-01  | 2023-12-31  |
    Given the following "account" data list exists
      | accountId | name   | accountGroup  | currency | openingDate | openingBalance  | fy   |
      | 2023_Cash | Cash   | ASSET         | SGD      | 2023-01-01  | 100.0           | 2023 |
      | Food      | Food   | EXPENSE       |          |             |                 |      |
    Given the following "journal entry" data list exists
      | journalId | txDate     | description | currency | amount | postDate   | debitAccountId | creditAccountId    |
      | 1         | 2023-02-01 | breakfast   | SGD      | 10.0   | 2023-02-01 | 2023_Cash      | Food               |
      | 2         | 2023-02-10 | lunch       | SGD      | 12.0   | 2023-02-10 | 2023_Cash      | Food               |
      | 3         | 2023-02-28 | dinner      | SGD      | 15.0   | 2023-02-28 | 2023_Cash      | Food               |
      | 4         | 2023-03-01 | breakfast   | SGD      | 10.0   | 2023-03-01 | 2023_Cash      | Food               |
      | 5         | 2023-03-02 | lunch       | SGD      | 12.0   | 2023-03-02 | 2023_Cash      | Food               |
      | 6         | 2023-03-16 | dinner      | SGD      | 15.0   | 2023-03-16 | 2023_Cash      | Food               |

  Scenario: List journal entries between dates
    When "/accountManager/2023/journals/2023-02-09/2023-03-02" is called with "GET"
    Then HttpStatus 200 is expected
    And the following data list is returned
      | journalId | txDate     | description | currency | amount | postDate   | debitAccountId | creditAccountId |
      | 2         | 2023-02-10 | lunch       | SGD      | 12.0   | 2023-02-10 | 2023_Cash      | Food            |
      | 3         | 2023-02-28 | dinner      | SGD      | 15.0   | 2023-02-28 | 2023_Cash      | Food            |
      | 4         | 2023-03-01 | breakfast   | SGD      | 10.0   | 2023-03-01 | 2023_Cash      | Food            |
      | 5         | 2023-03-02 | lunch       | SGD      | 12.0   | 2023-03-02 | 2023_Cash      | Food            |