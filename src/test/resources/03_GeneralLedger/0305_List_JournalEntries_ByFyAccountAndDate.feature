Feature: List journal entries by Date
  List journal entries by Date

  Background: Existing accounts in the database
    Given the following "financial year" data list exists
      | fy    | fromDate    | toDate      |
      | 2023  | 2023-01-01  | 2023-12-31  |
    Given the following "account" data list exists
      | accountId | name      | accountGroup  | currency | openingDate | openingBalance  | fy   |
      | 2023_Cash | Cash      | ASSET         | SGD      | 2023-01-01  | 100.0           | 2023 |
      | Food      | Food      | EXPENSE       |          |             |                 |      |
      | Transport | Transport | EXPENSE       |          |             |                 |      |
      | 2023_Card | Card      | LIABILITY     | SGD      | 2023-01-01  | 100.0           | 2023 |
    Given the following "journal entry" data list exists
      | journalId | txDate     | description | currency | amount | postDate   | debitAccountId | creditAccountId |
      | 1         | 2023-02-01 | breakfast   | SGD      | 10.0   | 2023-02-01 | Food           | 2023_Cash       |
      | 2         | 2023-02-02 | refund      | SGD      | 12.0   | 2023-02-10 | 2023_Cash      | Food            |
      | 3         | 2023-02-02 | snacks      | SGD      | 3.0    | 2023-02-10 | 2023_Cash      | 2023_Card       |
      | 4         | 2023-02-28 | dinner      | SGD      | 15.0   | 2023-02-28 | Food           | 2023_Cash       |
      | 5         | 2023-02-28 | taxi        | SGD      | 25.0   | 2023-02-28 | Transport      | 2023_Card       |
      | 6         | 2023-03-01 | breakfast   | SGD      | 10.0   | 2023-03-01 | Food           | 2023_Cash       |
      | 7         | 2023-03-01 | bus         | SGD      | 2.4    | 2023-03-02 | Transport      | 2023_Card       |
      | 8         | 2023-03-02 | lunch       | SGD      | 12.0   | 2023-03-02 | Food           | 2023_Cash       |
      | 9         | 2023-03-16 | dinner      | SGD      | 15.0   | 2023-03-16 | Food           | 2023_Cash       |
      | 10        | 2023-03-17 | bus         | SGD      | 1.2    | 2023-03-02 | Transport      | 2023_Cash       |


  Scenario: List journal entries between dates for cash
    When "/accountManager/2023/journals/Cash/2023-02-02/2023-03-16" is called with "GET"
    Then HttpStatus 200 is expected
    And the following data list is returned
      | journalId | txDate     | description | currency | amount | postDate   | debitAccountId | creditAccountId |
      | 2         | 2023-02-02 | refund      | SGD      | 12.0   | 2023-02-10 | 2023_Cash      | Food            |
      | 3         | 2023-02-02 | snacks      | SGD      | 3.0    | 2023-02-10 | 2023_Cash      | 2023_Card       |
      | 4         | 2023-02-28 | dinner      | SGD      | 15.0   | 2023-02-28 | Food           | 2023_Cash       |
      | 6         | 2023-03-01 | breakfast   | SGD      | 10.0   | 2023-03-01 | Food           | 2023_Cash       |
      | 8         | 2023-03-02 | lunch       | SGD      | 12.0   | 2023-03-02 | Food           | 2023_Cash       |
      | 9         | 2023-03-16 | dinner      | SGD      | 15.0   | 2023-03-16 | Food           | 2023_Cash       |

  Scenario: List journal entries between dates for transport
    When "/accountManager/2023/journals/Transport/2023-02-02/2023-03-16" is called with "GET"
    Then HttpStatus 200 is expected
    And the following data list is returned
      | journalId | txDate     | description | currency | amount | postDate   | debitAccountId | creditAccountId |
      | 5         | 2023-02-28 | taxi        | SGD      | 25.0   | 2023-02-28 | Transport      | 2023_Card       |
      | 7         | 2023-03-01 | bus         | SGD      | 2.4    | 2023-03-02 | Transport      | 2023_Card       |